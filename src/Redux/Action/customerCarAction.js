import { GET_CUSTOMER_CAR, GET_CUSTOMER_CAR_ID } from './type';

// 8. Axios API
var axios = require('axios');

// 9. getCustomerCar
function getCustomerCar() {
    let config = {
        method: 'get',
        url: 'https://rent-cars-api.herokuapp.com/customer/car',
        headers: {}
    };

    return (dispatch) => {
        axios(config)
            .then(function (response) {
                dispatch({
                    type: GET_CUSTOMER_CAR,
                    payload: response
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }
}

function getCustomerCarId(id) {

    let config = {
        method: 'get',
        url: 'https://rent-cars-api.herokuapp.com/customer/car/' + id,
        headers: {}
    };

    return (dispatch) => {
        axios(config)
            .then(function (response) {
                dispatch({
                    type: GET_CUSTOMER_CAR_ID,
                    payload: response
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }
}

// 10. export function
export { getCustomerCar, getCustomerCarId }
