import { GET_CUSTOMER_CAR, GET_CUSTOMER_CAR_ID } from "../Action/type";

// 11. initialState
const initialState = {
    dataCustomerCar: [],
    dataCustomerCarId: {},
}

// 12. Reducer
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_CUSTOMER_CAR:
            return {
                ...state,
                dataCustomerCar: action.payload.data
            }
            break;
        case GET_CUSTOMER_CAR_ID:
            return {
                ...state,
                dataCustomerCarId: action.payload.data
            }
            break;
        default:
            return state
    }
}