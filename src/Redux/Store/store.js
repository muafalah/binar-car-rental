import { applyMiddleware, compose, legacy_createStore } from "redux";
import thunk from "redux-thunk";
import combineReducers from "../Reducer/index"

// 2. middleware
const middlewareThunk = compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

// 1. store
// 14. call combineReducers & middlewareThunk
const store = legacy_createStore(combineReducers, middlewareThunk)

// 3. export store
export default store