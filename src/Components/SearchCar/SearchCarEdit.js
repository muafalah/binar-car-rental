import React, { useState } from 'react'
import { Col, Form, Row, Button } from 'react-bootstrap'
import style from './SearchCarEdit.module.css'

export const SearchCarEdit = ({ driver, date, time, passenger }) => {

    const [InputDriver, setInputDriver] = useState(driver)
    const [InputDate, setInputDate] = useState(date)
    const [InputTime, setInputTime] = useState(time)
    const [InputPassenger, setInputPassenger] = useState(passenger)

    const handleInput = (e) => {
        if (e.target.name === "driver") {
            setInputDriver(e.target.value)
        } else if (e.target.name === "date") {
            setInputDate(e.target.value)
        } else if (e.target.name === "time") {
            setInputTime(e.target.value)
        } else if (e.target.name === "passenger") {
            setInputPassenger(e.target.value)
        }
    }

    const handleSubmit = () => {
        if (InputDriver === "Pilih Tipe Driver" || InputDriver === undefined || InputDate === null || InputTime === null) {
            alert("Kolom Pencarian belum diisi")
        } else {
            if (InputPassenger === undefined) {
                History("/search/result/driver=" + InputDriver + "&date=" + InputDate + "&time=" + InputTime + "&passenger=" + 0)
            } else {
                History("/search/result/driver=" + InputDriver + "&date=" + InputDate + "&time=" + InputTime + "&passenger=" + InputPassenger)
            }
        }
    }

    return (
        <div>
            <Form className={'p-4 ' + style.searchbox}>
                <Row className={'gap-lg-0 gap-3'}>
                    <Col lg={11} md={12}>
                        <Row>
                            <Col lg={3} md={6} className={'mt-2'}>
                                <Form.Group>
                                    <Form.Label>Tipe Driver</Form.Label>
                                    <Form.Select type="dropdown" placeholder="Pilih Tipe Driver" name="driver" onChange={(e) => { handleInput(e) }} value={InputDriver}>
                                        <option>Pilih Tipe Driver</option>
                                        <option value="supir">Dengan Supir</option>
                                        <option value="tanpa-supir">Tanpa Supir(lepas kunci)</option>
                                    </Form.Select>
                                </Form.Group>
                            </Col>
                            <Col lg={3} md={6} className={'mt-2'}>
                                <Form.Group>
                                    <Form.Label>Tanggal</Form.Label>
                                    <Form.Control type="date" placeholder="Pilih Tanggal" name="date" onChange={(e) => { handleInput(e) }} value={InputDate} />
                                </Form.Group>
                            </Col>
                            <Col lg={3} md={6} className={'mt-2'}>
                                <Form.Group>
                                    <Form.Label>Waktu Jemput/Ambil</Form.Label>
                                    <Form.Control type="time" placeholder="Pilih Waktu" name="time" onChange={(e) => { handleInput(e) }} value={InputTime} />
                                </Form.Group>
                            </Col>
                            <Col lg={3} md={6} className={'mt-2'}>
                                <Form.Group>
                                    <Form.Label>Jumlah Penumpang</Form.Label>
                                    <Form.Control type="number" placeholder="Jumlah Penumpang" name="passenger" onChange={(e) => { handleInput(e) }} value={InputPassenger} />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={1} md={12} className={'mt-auto d-grid gap-2 '}>
                        <Button variant="outline-primary" type="submit" onClick={handleSubmit}>
                            Edit
                        </Button>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}
