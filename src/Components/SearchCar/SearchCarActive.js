import React, { useState } from 'react'
import { Col, Form, Row, Button } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import style from './SearchCarActive.module.css'

export const SearchCarActive = () => {

    const History = useNavigate()

    const [InputDriver, setInputDriver] = useState()
    const [InputDate, setInputDate] = useState()
    const [InputTime, setInputTime] = useState()
    const [InputPassenger, setInputPassenger] = useState()

    const handleInput = (e) => {
        if (e.target.name === "driver") {
            setInputDriver(e.target.value)
        } else if (e.target.name === "date") {
            setInputDate(e.target.value)
        } else if (e.target.name === "time") {
            setInputTime(e.target.value)
        } else if (e.target.name === "passenger") {
            setInputPassenger(e.target.value)
        }
    }

    const handleSubmit = () => {
        if (InputDriver === "Pilih Tipe Driver" || InputDriver === undefined || InputDate === null || InputTime === null) {
            alert("Kolom Pencarian belum diisi")
        } else {
            if (InputPassenger === undefined) {
                History("/search/result/driver=" + InputDriver + "&date=" + InputDate + "&time=" + InputTime + "&passenger=" + 0)
            } else {
                History("/search/result/driver=" + InputDriver + "&date=" + InputDate + "&time=" + InputTime + "&passenger=" + InputPassenger)
            }
        }
    }

    return (
        <div>
            <Form className={'p-4 ' + style.searchbox}>
                <Row className={'gap-lg-0 gap-3'}>
                    <Col lg={10} md={12}>
                        <Row>
                            <Col lg={3} md={6} className={'mt-2'}>
                                <Form.Group>
                                    <Form.Label>Tipe Driver</Form.Label>
                                    <Form.Select type="dropdown" placeholder="Pilih Tipe Driver" name="driver" onChange={(e) => { handleInput(e) }}>
                                        <option>Pilih Tipe Driver</option>
                                        <option value="supir">Dengan Supir</option>
                                        <option value="tanpa-supir">Tanpa Supir(lepas kunci)</option>
                                    </Form.Select>
                                </Form.Group>
                            </Col>
                            <Col lg={3} md={6} className={'mt-2'}>
                                <Form.Group>
                                    <Form.Label>Tanggal</Form.Label>
                                    <Form.Control type="date" placeholder="Pilih Tanggal" name="date" onChange={(e) => { handleInput(e) }} />
                                </Form.Group>
                            </Col>
                            <Col lg={3} md={6} className={'mt-2'}>
                                <Form.Group>
                                    <Form.Label>Waktu Jemput/Ambil</Form.Label>
                                    <Form.Control type="time" placeholder="Pilih Waktu" name="time" onChange={(e) => { handleInput(e) }} />
                                </Form.Group>
                            </Col>
                            <Col lg={3} md={6} className={'mt-2'}>
                                <Form.Group>
                                    <Form.Label>Jumlah Penumpang</Form.Label>
                                    <Form.Control type="number" placeholder="Jumlah Penumpang" name="passenger" onChange={(e) => { handleInput(e) }} />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={2} md={12} className={'mt-auto d-grid gap-2 '}>
                        <Button className="button-secondary" type="submit" onClick={handleSubmit}>
                            Cari Mobil
                        </Button>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}
