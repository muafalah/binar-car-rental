import React, { useState } from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap'
import style from './CardCarDetail.module.css'
import IconPeople from '../../Assets/icon/CardCar/icon-people.svg'
import IconSetting from '../../Assets/icon/CardCar/icon-setting.svg'
import IconCalendar from '../../Assets/icon/CardCar/icon-calendar.svg'

export const CardCarDetail = ({ id, image, name, price, category, createdAt }) => {

    const changePrice = () => {
        let separator

        let number_string = price.toString(),
            sisa = number_string.length % 3,
            rupiah = number_string.substr(0, sisa),
            ribuan = number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        return rupiah
    }

    const changeCreatedAt = () => {
        return (createdAt.substring(0, 4))
    }

    const changeCategory = () => {
        if (category === "small") {
            return "4"
        } else if (category === "medium") {
            return "7"
        } else if (category === "large") {
            return "9"
        }
    }

    return (
        <div>
            <div>
                <Card className={'p-2 '}>
                    <Card.Body>
                        <Card.Img variant="top" src={image} height="200vh" />
                    </Card.Body>
                    <Card.Body>
                        <Card.Title className={'m-0 p-0 mb-1 ' + style.name_carddetail}>{name}</Card.Title>
                        <span className={'me-3 ' + style.desc_carddetail}><img src={IconPeople} width="16px" height="16px" className={'me-1 '} /> {changeCategory()} orang</span>
                        <span className={'me-3 ' + style.desc_carddetail}><img src={IconSetting} width="16px" height="16px" className={'me-1 '} /> Manual</span>
                        <span className={'me-3 ' + style.desc_carddetail}><img src={IconCalendar} width="16px" height="16px" className={'me-1 '} /> Tahun {changeCreatedAt()}</span>
                        <hr />
                        <Row>
                            <Col md={6}>
                                Total
                            </Col>
                            <Col md={6}>
                                <Card.Text className={'m-0 p-0 mb-1 text-end ' + style.price_carddetail}>Rp {changePrice()}</Card.Text>
                            </Col>
                        </Row>
                        <a href="#" className={'d-grid gap mt-3 '}><Button className={'button-secondary '}>Lanjutkan Pembayaran</Button></a>
                    </Card.Body>
                </Card>
            </div>
        </div>
    )
}
