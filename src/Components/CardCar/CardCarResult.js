import React from 'react'
import { Card, Button } from 'react-bootstrap'
import style from './CardCarResult.module.css'
import IconPeople from '../../Assets/icon/CardCar/icon-people.svg'
import IconSetting from '../../Assets/icon/CardCar/icon-setting.svg'
import IconCalendar from '../../Assets/icon/CardCar/icon-calendar.svg'

export const CardCarResult = ({ id, image, name, price, category, createdAt }) => {

    const changePrice = () => {
        let separator

        let number_string = price.toString(),
            sisa = number_string.length % 3,
            rupiah = number_string.substr(0, sisa),
            ribuan = number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        return rupiah
    }

    const changeCreatedAt = () => {
        return (createdAt.substring(0, 4))
    }

    const changeCategory = () => {
        if (category === "small") {
            return "4"
        } else if (category === "medium") {
            return "7"
        } else if (category === "large") {
            return "9"
        }
    }

    return (
        <div className={style.cardcar}>
            <Card className={'p-2 '}>
                <Card.Body>
                    <Card.Img variant="top" src={image} height="170vh" />
                </Card.Body>
                <Card.Body>
                    <Card.Title className={'m-0 p-0 mb-1 ' + style.name_card}>{name}</Card.Title>
                    <Card.Text className={'m-0 p-0 mb-1 ' + style.price_card}>Rp {changePrice()} / hari</Card.Text>
                    <Card.Text className={'m-0 p-0 mb-2 ' + style.desc_card}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </Card.Text>
                    <div className={'d-flex align-items-center mb-1 ' + style.desc_card}><img src={IconPeople} width="20px" height="20px" className={'me-2 '} /> {changeCategory()} orang</div>
                    <div className={'d-flex align-items-center mb-1 ' + style.desc_card}><img src={IconSetting} width="20px" height="20px" className={'me-2 '} /> Manual</div>
                    <div className={'d-flex align-items-center mb-1 ' + style.desc_card}><img src={IconCalendar} width="20px" height="20px" className={'me-2 '} /> Tahun {changeCreatedAt()}</div>
                    <a href={"../detail/" + id} className={'d-grid gap mt-3 '}><Button className={'button-secondary '}>Pilih Mobil</Button></a>
                </Card.Body>
            </Card>
        </div>
    )
}
