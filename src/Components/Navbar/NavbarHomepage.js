import React from 'react'
import style from './NavbarHomepage.module.css'
import { Container, Nav, Navbar, Button } from 'react-bootstrap'
import Logo from '../../Assets/img/Navbar/img-logo.svg'

export const NavbarHomepage = () => {
    return (
        <div>
            <Navbar className={style.bg_navbar} expand="lg">
                <Container>
                    <Navbar.Brand href="/"><img src={Logo} alt="Binar Car Rental" /></Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className={'ms-auto gap-2 '}>
                            <Nav.Link className={'my-auto ' + style.nav_link} href="#">Our Service</Nav.Link>
                            <Nav.Link className={'my-auto ' + style.nav_link} href="#">Why Us</Nav.Link>
                            <Nav.Link className={'my-auto ' + style.nav_link} href="#">Testimonial</Nav.Link>
                            <Nav.Link className={'my-auto ' + style.nav_link} href="#">FAQ</Nav.Link>
                            <Nav.Link className={'my-auto ' + style.nav_link} href="#"><Button className={'button-secondary '}>Register</Button></Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}
