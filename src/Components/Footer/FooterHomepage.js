import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import style from './FooterHomepage.module.css'
import IconFacebook from '../../Assets/icon/Footer/icon-facebook.svg'
import IconInstagram from '../../Assets/icon/Footer/icon-instagram.svg'
import IconTwitter from '../../Assets/icon/Footer/icon-twitter.svg'
import IconMail from '../../Assets/icon/Footer/icon-mail.svg'
import IconTwitch from '../../Assets/icon/Footer/icon-twitch.svg'
import Logo from '../../Assets/img/Footer/img-logo.svg'

export const FooterHomepage = () => {
    return (
        <div>
            <Container className={'pt-5 pb-5 '}>
                <Row>
                    <Col md={3} className={'pt-3 '}>
                        <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                        <p>binarcarrental@gmail.com</p>
                        <p>081-233-334-808</p>
                    </Col>
                    <Col md={2} className={'pt-3 '}>
                        <p><a className={style.nav_footer} href="/#OurService-Homepage"><b>Our Service</b></a></p>
                        <p><a className={style.nav_footer} href="/#WhyUs-Homepage"><b>Why Us</b></a></p>
                        <p><a className={style.nav_footer} href="/#Tesimonial-Homepage"><b>Testimonial</b></a></p>
                        <p><a className={style.nav_footer} href="/#FAQ-Homepage"><b>FAQ</b></a></p>
                    </Col>
                    <Col md={4} className={'pt-3 '}>
                        <p>Connect with us</p>
                        <a href="#" className={'me-2 '}><img src={IconFacebook} width="32px" height="32px" /></a>
                        <a href="#" className={'me-2 '}><img src={IconInstagram} width="32px" height="32px" /></a>
                        <a href="#" className={'me-2 '}><img src={IconTwitter} width="32px" height="32px" /></a>
                        <a href="#" className={'me-2 '}><img src={IconMail} width="32px" height="32px" /></a>
                        <a href="#" className={'me-2 '}><img src={IconTwitch} width="32px" height="32px" /></a>
                    </Col>
                    <Col md={3} className={'pt-3 '}>
                        <p>Copyright Binar 2022</p>
                        <img src={Logo} alt="Binar Car Rental" />
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
