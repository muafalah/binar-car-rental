import React from 'react'
import { Route, Routes } from 'react-router-dom'
import DetailCar from '../Views/DetailCar/DetailCar'
import Homepage from '../Views/Homepage/Homepage'
import ResultCar from '../Views/ResultCar/ResultCar'

export const Routers = () => {
    return (
        <Routes>
            <Route path="/" element={<Homepage view="homepage" />} />
            <Route path="/search" element={<Homepage view="search" />} />
            <Route path="/search/result/driver=:driver&date=:date&time=:time&passenger=:passenger" element={<ResultCar view="result" />} />
            <Route path="/search/detail/:id" element={<DetailCar />} />
        </Routes>
    )
}
