import React, { useEffect } from 'react'
import { Accordion, Col, Container, Row, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { useParams } from 'react-router-dom'
import { CardCarDetail } from '../../Components/CardCar/CardCarDetail'
import { FooterHomepage } from '../../Components/Footer/FooterHomepage'
import { NavbarHomepage } from '../../Components/Navbar/NavbarHomepage'
import { SearchCarDisable } from '../../Components/SearchCar/SearchCarDisable'
import { getCustomerCarId } from '../../Redux/Action/customerCarAction'
import style from './DetailCar.module.css'

const DetailCar = (props) => {

    const { id } = useParams()

    useEffect(() => {
        props.getApi(id)
    }, [])

    return (
        <div>
            <section id="Navbar-Search">
                <NavbarHomepage />
            </section>
            <div className={style.blank_result}></div>
            <section id="Detail-Search">
                <Container>
                    <div className={'ms-lg-5 me-lg-5'}>
                        <SearchCarDisable />
                        <Row className={'mt-4'}>
                            <Col md={7} className={'mt-3 '}>
                                <div className={'p-4 ' + style.searchbox_detail}>
                                    <h5>Tentang Paket</h5>
                                    Include
                                    <ul>
                                        <li className={style.text_detail}>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
                                        <li className={style.text_detail}>Sudah termasuk bensin selama 12 jam</li>
                                        <li className={style.text_detail}>Sudah termasuk Tiket Wisata</li>
                                        <li className={style.text_detail}>Sudah termasuk pajak</li>
                                    </ul>
                                    Exclude
                                    <ul>
                                        <li className={style.text_detail}>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                        <li className={style.text_detail}>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                        <li className={style.text_detail}>Tidak termasuk akomodasi penginapan</li>
                                    </ul>
                                    <Accordion defaultActiveKey="0">
                                        <Accordion.Item className={style.item_accordion} eventKey="0">
                                            <Accordion.Header className={style.header_accordion}> <h5>Refund, Reschedule, Overtime</h5></Accordion.Header>
                                            <Accordion.Body className={style.body_accordion}>
                                                <ul>
                                                    <li className={style.text_detail}>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                    <li className={style.text_detail}>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                    <li className={style.text_detail}>Tidak termasuk akomodasi penginapan</li>
                                                    <li className={style.text_detail}>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                    <li className={style.text_detail}>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                    <li className={style.text_detail}>Tidak termasuk akomodasi penginapan</li>
                                                    <li className={style.text_detail}>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                    <li className={style.text_detail}>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                    <li className={style.text_detail}>Tidak termasuk akomodasi penginapan</li>
                                                </ul>
                                            </Accordion.Body>
                                        </Accordion.Item>
                                    </Accordion>
                                </div>
                            </Col>
                            <Col md={5} className={'mt-3 '}>
                                {props.data.id == null ? null : <CardCarDetail id={props.data.id} image={props.data.image} name={props.data.name} price={props.data.price} category={props.data.category} createdAt={props.data.createdAt} />}
                            </Col>
                            <Col md={12} className={'mt-5 d-flex justify-content-center '}>
                                <Button className={'button-secondary'}>Lanjutkan Pembayaran</Button>
                            </Col>
                        </Row>
                    </div>
                </Container>
            </section>
            <section id="Footer-Search">
                <FooterHomepage />
            </section>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        data: state.customerCar.dataCustomerCarId
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        getApi: (id) => dispatch(getCustomerCarId(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailCar)