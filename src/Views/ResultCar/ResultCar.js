import React, { useEffect } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { useParams } from 'react-router-dom'
import { CardCarResult } from '../../Components/CardCar/CardCarResult'
import { FooterHomepage } from '../../Components/Footer/FooterHomepage'
import { NavbarHomepage } from '../../Components/Navbar/NavbarHomepage'
import { SearchCarEdit } from '../../Components/SearchCar/SearchCarEdit'
import { getCustomerCar } from '../../Redux/Action/customerCarAction'
import style from './ResultCar.module.css'

const ResultCar = (props) => {

    useEffect(() => {
        props.getApi()
    }, [])

    const { driver, date, time, passenger } = useParams()

    const filterCar = () => {
        return props.data?.map((value, index) => {
            if (value.status === false) {
                return (
                    <Col lg={4} md={6} className={'pt-2 pb-2 '} key={index}>
                        <CardCarResult id={value.id} image={value.image} name={value.name} price={value.price} category={value.category} createdAt={value.createdAt} />
                    </Col>
                )
            }
        })
    }

    return (
        <div>
            <section id="Navbar-Search">
                <NavbarHomepage />
            </section>
            <div className={style.blank_result}></div>
            <section id="Result-Search">
                <Container>
                    <div className={'ms-lg-5 me-lg-5'}>
                        <SearchCarEdit driver={driver} date={date} time={time} passenger={passenger} />
                        <Row className={'mt-4 '}>
                            {filterCar()}
                        </Row>
                    </div>
                </Container>
            </section>
            <section id="Navbar-Search">
                <FooterHomepage />
            </section>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        data: state.customerCar.dataCustomerCar
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getApi: () => dispatch(getCustomerCar())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ResultCar)