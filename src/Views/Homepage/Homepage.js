import React from 'react'
import style from './Homepage.module.css'
import { FooterHomepage } from '../../Components/Footer/FooterHomepage'
import { NavbarHomepage } from '../../Components/Navbar/NavbarHomepage'
import { Col, Container, Row, Button, Card, Accordion } from 'react-bootstrap'
import ImageHero from '../../Assets/img/Homepage/img-hero.svg'
import ImageOurService from '../../Assets/img/Homepage/img-service.svg'
import IconChecklist from '../../Assets/icon/Homepage/icon-checklist.svg'
import IconComplete from '../../Assets/icon/Homepage/icon-complete.svg'
import IconPrice from '../../Assets/icon/Homepage/icon-price.svg'
import Icon24Hour from '../../Assets/icon/Homepage/icon-24hrs.svg'
import IconProfessional from '../../Assets/icon/Homepage/icon-professional.svg'
import { SearchCarActive } from '../../Components/SearchCar/SearchCarActive'

const Homepage = ({ view }) => {

    const viewHomepage = () => {
        return (
            <div>
                <section id="Hero-Homepage" className={'mb-5 ' + style.hero_homepage}>
                    <Container>
                        <Row>
                            <Col md={6} className={'my-auto pb-3 '}>
                                <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                                <p className={'col-lg-10 ' + style.desc_hero}>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                                <a href="/search"><Button className={'button-secondary '}>Mulai Sewa Mobil</Button></a>
                            </Col>
                            <Col md={6} className={'mt-5 '}>
                                <img src={ImageHero} className={style.hero_image} alt="Hero Image" />
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section id="OurService-Homepage" className={'pt-3 pb-3 '}>
                    <Container>
                        <Row>
                            <Col lg={5} md={6} sm={12} className={'offset-lg-1 my-auto pt-3 pb-3 '}>
                                <img src={ImageOurService} width="90%" alt="Our Service" />
                            </Col>
                            <Col lg={5} md={6} sm={12} className={'pt-3 pb-3 '}>
                                <h3>Best Car Rental for any kind of trip in (Lokasimu)!</h3>
                                <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                                <p className={'d-flex align-items-center '}><img src={IconChecklist} className={'me-3 '} width="24px" height="24px" />Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                                <p className={'d-flex align-items-center '}><img src={IconChecklist} className={'me-3 '} width="24px" height="24px" />Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                                <p className={'d-flex align-items-center '}><img src={IconChecklist} className={'me-3 '} width="24px" height="24px" />Sewa Mobil Jangka Panjang Bulanan</p>
                                <p className={'d-flex align-items-center '}><img src={IconChecklist} className={'me-3 '} width="24px" height="24px" />Gratis Antar - Jemput Mobil di Bandara</p>
                                <p className={'d-flex align-items-center '}><img src={IconChecklist} className={'me-3 '} width="24px" height="24px" />Layanan Airport Transfer / Drop In Out</p>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section id="WhyUs-Homepage" className={'pt-3 pb-3 '}>
                    <Container>
                        <Row>
                            <Col md={12} className={'pt-lg-3 pb-lg-3 '}>
                                <h3 className={'text-center text-md-start '}>Why Us?</h3>
                                <p className={'text-center text-md-start '}>Mengapa harus pilih Binar Car Rental?</p>
                            </Col>
                            <Col md={12} className={'pt-3 pb-3 '}>
                                <Row>
                                    <Col lg={3} md={6} className={'mb-3 '}>
                                        <Card className={'p-0 '}>
                                            <Card.Body className={'p-4 '}>
                                                <img src={IconComplete} alt="Icon Complete" className={'mb-3 '} width="32px" height="32px" />
                                                <Card.Title className={'mb-3 '}><h5>Mobil Lengkap</h5></Card.Title>
                                                <Card.Text>
                                                    Tersedia banyak sekali pilihan mobil, masih dalam kondisi baru, bersih dan terawat
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                    <Col lg={3} md={6} className={'mb-3 '}>
                                        <Card className={'p-0 '}>
                                            <Card.Body className={'p-4 '}>
                                                <img src={IconPrice} alt="Icon Complete" className={'mb-3 '} width="32px" height="32px" />
                                                <Card.Title className={'mb-3 '}><h5>Harga Murah</h5></Card.Title>
                                                <Card.Text>
                                                    Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                    <Col lg={3} md={6} className={'mb-3 '}>
                                        <Card className={'p-0 '}>
                                            <Card.Body className={'p-4 '}>
                                                <img src={Icon24Hour} alt="Icon Complete" className={'mb-3 '} width="32px" height="32px" />
                                                <Card.Title className={'mb-3 '}><h5>Layanan 24 Jam</h5></Card.Title>
                                                <Card.Text>
                                                    Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                    <Col lg={3} md={6} className={'mb-3 '}>
                                        <Card className={'p-0 '}>
                                            <Card.Body className={'p-4 '}>
                                                <img src={IconProfessional} alt="Icon Complete" className={'mb-3 '} width="32px" height="32px" />
                                                <Card.Title className={'mb-3 '}><h5>Sopir Professional</h5></Card.Title>
                                                <Card.Text>
                                                    Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section id="Testimonial-Homepage" className={'pt-3 pb-3 '}>
                    <Container>
                        <Row>
                            <Col md={12} className={'pt-lg-3 pb-lg-3 '}>
                                <h3 className={'text-center '}>Testimonial</h3>
                            </Col>
                            <Col md={12} className={'pt-3 pb-3 '}>
                                Testimonial
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section id="CTABanner-Homepage" className={'pt-3 pb-3 '}>
                    <Container>
                        <Row className={'pt-3 pb-3'}>
                            <Col md={12}>
                                <Card className={'text-center pt-5 pb-5 pe-3 ps-3 ' + style.card_cta}>
                                    <Card.Body>
                                        <Card.Title><h1 className={'mb-3 '} style={{ color: 'white' }}>Sewa Mobil di (Lokasimu) Sekarang</h1></Card.Title>
                                        <Card.Text style={{ color: 'white' }} className={'mb-5 '}>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <br />tempor incididunt ut labore et dolore magna aliqua.
                                        </Card.Text>
                                        <a href="/search"><Button className={'button-secondary '}>Mulai Sewa Mobil</Button></a>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section id="FAQ-Homepage" className={'pt-3 pb-3 '}>
                    <Container>
                        <Row className={'pt-3 pb-3 '}>
                            <Col md={5}>
                                <h3 className={'text-center text-md-start '}>Frequently Asked Question</h3>
                                <p className={'text-center text-md-start '}>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                            </Col>
                            <Col md={7} >
                                <Accordion>
                                    <Accordion.Item className={'mb-3 '} eventKey="0">
                                        <Accordion.Header>Apa saja syarat yang dibutuhkan?</Accordion.Header>
                                        <Accordion.Body>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                            commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                            est laborum.
                                        </Accordion.Body>
                                    </Accordion.Item>
                                </Accordion>
                                <Accordion>
                                    <Accordion.Item className={'mb-3 '} eventKey="1">
                                        <Accordion.Header>Berapa hari minimal sewa mobil lepas kunci?</Accordion.Header>
                                        <Accordion.Body>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                            commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                            est laborum.
                                        </Accordion.Body>
                                    </Accordion.Item>
                                </Accordion>
                                <Accordion>
                                    <Accordion.Item className={'mb-3 '} eventKey="2">
                                        <Accordion.Header>Sebaiknya berapa hari sebelumnya untuk booking sewa mobil?</Accordion.Header>
                                        <Accordion.Body>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                            commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                            est laborum.
                                        </Accordion.Body>
                                    </Accordion.Item>
                                </Accordion>
                                <Accordion>
                                    <Accordion.Item className={'mb-3 '} eventKey="3">
                                        <Accordion.Header>Apakah ada biaya antar-jemput?</Accordion.Header>
                                        <Accordion.Body>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                            commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                            est laborum.
                                        </Accordion.Body>
                                    </Accordion.Item>
                                </Accordion>
                                <Accordion>
                                    <Accordion.Item className={'mb-3 '} eventKey="4">
                                        <Accordion.Header>Bagaimana jika terjadi kecelakaan?</Accordion.Header>
                                        <Accordion.Body>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                            commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                            est laborum.
                                        </Accordion.Body>
                                    </Accordion.Item>
                                </Accordion>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </div>
        )
    }

    const viewSearch = () => {
        return (
            <div>
                <section id="Hero-Homepage" className={'mb-5 ' + style.hero_homepage}>
                    <Container>
                        <Row>
                            <Col md={6} className={'my-auto '}>
                                <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                                <p className={'col-lg-10 ' + style.desc_hero}>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                            </Col>
                            <Col md={6} className={'mt-5 '}>
                                <img src={ImageHero} className={style.hero_image} alt="Hero Image" />
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section id="Search-Homepage">
                    <Container>
                        <div className={'ms-lg-5 me-lg-5'}>
                            <SearchCarActive />
                        </div>
                    </Container>
                </section>
            </div>
        )
    }

    return (
        <div>
            <section id="Navbar-Homepage">
                <NavbarHomepage />
            </section>
            {view === "homepage" ? viewHomepage() : null}
            {view === "search" ? viewSearch() : null}
            <section id="Footer-Homepage">
                <FooterHomepage />
            </section>
        </div>
    )
}

export default Homepage